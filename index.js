// console.log("Hello World")

// Array Methods
// JS has built-in functions and methods for arrays.

// [SECTION] Mutator Methods

let fruits = ["Apple", "Orange", "Kiwi", "Dragonfruit"]

// push() method
// will add element to the end part of the array
/*
	Syntax:
		arrayName.push()
*/

console.log("Current Array: ")
console.log(fruits)

let fruitsLength = fruits.push("Mango")
console.log(fruitsLength)
console.log("Mutated array from push() method")
console.log(fruits)

// Adding multiple elements to an array
fruits.push("Avocado", "Guava")
console.log("Mutated array from push() method")
console.log(fruits)

// pop() method
// will remove array element at the end part of the array
let removedFruit = fruits.pop()
console.log(removedFruit)
console.log("Mutated array from pop() method")
console.log(fruits)

// unshift() method
// add elements in the beginning of an array
fruits.unshift("Lime", "Banana")
console.log("Mutated array from unshift() method")
console.log(fruits)

// shift() method
// removes an element in the beginning part of an array
let anotherFruit = fruits.shift()
console.log(anotherFruit)
console.log("Mutated array from shift() method")
console.log(fruits)

// splice() method
// simultaneously removes an element from a specified index number and adds elements
/*
	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/
fruits.splice(1, 2, "Lime", "Cherry")
console.log("Mutated array from splice() method")
console.log(fruits)

// sort() method
// rearranges the array elements in alphanumeric order
fruits.sort()
console.log("Mutated array from sort() method")
console.log(fruits)

// reverse()
// reverse the order of the array elements
fruits.reverse()
console.log("Mutated array from reverse() method")
console.log(fruits)

// [SECTION] Non-Mutator Methods
// unlike the mutator methods, non-mutator methods cannot modify the array

let countries = ["USA", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"]
console.log(countries)

// indexOf() method
// returns the index number of the first matching element in the array. 
// if there are no matching element, it will return -1.
/*
	Syntax:
		arrayName.indexOf(searchValue, fromIndex)
*/

let firstIndex = countries.indexOf("PH")
console.log("Result of indexOf() method: " + firstIndex)

let invalidCountry = countries.indexOf("BR")
console.log("Result of indexOf() method: " + invalidCountry)

// lastIndexOf() method
// returns the index number of the last matching element in the array. 
/*
	Syntax:
		arrayName.lastIndexOf(searchValue, fromIndex)
*/
let lastIndex = countries.lastIndexOf("PH")
console.log("Result of lastIndexOf() method: " + lastIndex)

// Getting the index number starting from a specified index
let lastIndexStart = countries.lastIndexOf("PH", 4)
console.log("Result of lastIndexOf() method: " + lastIndexStart)

// slice() method
// protions/slices elements from an array and return a new array
/*
	Syntax: 
		arrayName.slice(startingIndex, endingIndex)	
*/
let slicedArrayA = countries.slice(2)
console.log("Result from slice() method: ")
console.log(slicedArrayA)

let slicedArrayB = countries.slice(2, 4)
console.log("Result from slice() method: ")
console.log(slicedArrayB)

// slicing off elements staring from the end part of the array
let slicedArrayC = countries.slice(-3)
console.log("Result from slice() method: ")
console.log(slicedArrayC)

// toString() method
// returns an array as a string separated by a comma
/*
	Syntax:
		arrayName.toString()
*/
let stringArray = countries.toString()
console.log("Result from toString() method: ")
console.log(stringArray)

// cancat() method
// Combines two arrays and returns the combined result
/*
	Syntax:
		arrayA.concat(arrayB)
*/

let taskArray1 = ["drink HTML", "eat JS"]
let taskArray2 = ["inhale CSS", "breathe SASS"]
let taskArray3 = ["get Git", "be Node"]

let tasks = taskArray1.concat(taskArray2)
console.log("Result from concat() method: ")
console.log(tasks)

// combining multiple arrays
let allTasks = taskArray1.concat(taskArray2, taskArray3)
console.log("Result from concat() method: ")
console.log(allTasks)

// combining arrays with elements
let combinedTasks = taskArray1.concat("smell Express", "throw React")
console.log("Result from concat() method: ")
console.log(combinedTasks)

// join() method
// returns n array as a tring seperated by specified separator string
/*
	Syntax:
		arrayName.join('separator string')
*/

let users = ["John", "Jane", "Joe", "Robert"]

console.log(users.join());
console.log(users.join(""));
console.log(users.join(" - "));

// iteration methods

// forEach() method
// similar to a for loop that iterates on each array elements
/*
	Syntax:
		arrayName.forEach(function(indiv_element){ statement }) 
*/

allTasks.forEach(function(task) {
	console.log(task)
})

// using forEach with conditional Statements

let filteredTasks = []

allTasks.forEach(function(task) {
	if(task.length > 10) {
		filteredTasks.push(task)
	}
})

console.log("Result of filteredTasks:")
console.log(filteredTasks)

// map() method This is useful for performing tasks where mutating/chaining the elements are required.
/*
	Syntax:
		let/const resultArray = arrayName.map(function())
*/

let numbers = [1, 2, 3, 4, 5]

let numberMap = numbers.map(function(number){
	return number * number
})

console.log("Original Array: ")
console.log(numbers) // original array is unaffected by the map()

console.log("Result from map() method")
console.log(numberMap) // a new array is returned and stored in a variable

// map() vs forEach()
let numberForEach = numbers.forEach(function(number) {
	return number * number
})

console.log(numberForEach) // undefined
// forEach() loops over all items in the array just like map(), but the forEach() does not return a new array.

// every() method
// checks if all elements in an array meets the given condition
/*
	Syntax:
		let/const resultArray = arrayName.every(function() { condition })
*/

let allValid = numbers.every(function(number) {
	return (number < 3)
})

console.log("result from every() method")
console.log(allValid)

// some() method
// checks if atleast one element in an array meets the given condition
/*
	Syntax:
		let/const resultArray = arrayName.some(function() { condition })
*/

let someValid = numbers.some(function(number) {
	return (number < 3)
})

console.log("result from some() method")
console.log(someValid)

// filter() method
// returns new array that contains element which meets the given condition
// returns an empty array if no element meets the condition
/*
	Syntax:
		let/const resultArray = arrayName.filter(function(indivElement) {Condition})
*/

let filteredValid = numbers.filter(function(number){
	return number < 3
})

console.log("Result from filter method")
console.log(filteredValid)

// includes() method
// checks if the argument passed can be found in the array
/*
	Syntax:
		arrayName.includes(argumentToFind)
*/
let products = ["Mouse", "Keyboard", "Laptop", "Monitor"]

let productFound1 = products.includes("Mouse")
console.log(productFound1) //true

let productFound2 = products.includes("Headset")
console.log(productFound2) //false

// combined filter() and includes() method
let filteredProduct = products.filter(function(product) {
	return product.toLowerCase().includes("a")
})
console.log(filteredProduct)

// reduce() method
// eveluates elements from left to right and returns/reduces the array into a single value
/*
	Syntax:
		let/const resultArray = arrayName.reduce(function(accumulator, currentValue){operation})
*/
let iteration = 0

let reducedArray = numbers.reduce(function(x, y) {
	console.warn("Current iteration: "+ ++iteration)
	console.log("accumulator: " + x)
	console.log("currentValue: " + y)

	// operation to reduce the array into a single value
	return x + y
})

console.log("Result of reduced method: " + reducedArray)

let list = ["Hello", "Again", "World"]

let reducedJoin = list.reduce(function(x, y) {
	return x + " " + y
})
console.log("Result of reduce() method: " + reducedJoin)


